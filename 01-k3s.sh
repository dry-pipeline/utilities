#! /bin/bash

# Install the k3d tool from Homebrew
brew install k3d

# Create a k3s cluster named k3s
k3d cluster create k3s --api-port 6550 -p "8081:80@loadbalancer" --agents 2

kubectx k3d-k3s
