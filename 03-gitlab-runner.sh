#! /bin/bash
brew install helm

helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install gitlab-runner --namespace gitlab-runner -f ./gitlab-runner/values.yaml gitlab/gitlab-runner --create-namespace
