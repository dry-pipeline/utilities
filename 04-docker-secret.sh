#! /bin/bash

read -p "Docker Username: " username
echo "Docker Password: "
read -s password
read -p "Docker Email: " email
read -p "Docker Registry: " registry


kubectl create ns dev
kubectl delete secret regcred --namespace dev
kubectl create secret docker-registry regcred \
    --docker-server=$registry \
    --docker-username=$username --docker-password=$password \
    --docker-email=$email \
    --namespace dev

kubectl create ns prod
kubectl delete secret regcred --namespace prod
kubectl create secret docker-registry regcred \
    --docker-server=$registry \
    --docker-username=$username --docker-password=$password \
    --docker-email=$email \
    --namespace prod
